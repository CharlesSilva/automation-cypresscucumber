import { Given, And, When, Then } from "cypress-cucumber-preprocessor/steps";
import Home from "../page_objects/usersPage"
const home = new Home()

Given('acesso o site itera-qa', () =>{
    home.accessWeb();
})
And('seleciono para acessar via login', () =>{
    home.loginScreen();
})
And('acesso com usuario {string} e {string} entrando na home Dashboard', (User,Password) =>{
    home.typeUser(User, Password);
    home.submit();
})
And('em Dashboard faço um cadastro de cliente', () =>{
    home.createNew()
})
When('faço a pesquisa do cliente cadastrado', () =>{
    home.searchCustomer()
})
Then('verifico que exibe o cliente na tabela de Detalhes de cliente', () =>{
    home.searchValidation()
})
