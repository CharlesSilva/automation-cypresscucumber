import { Given, And, When, Then } from "cypress-cucumber-preprocessor/steps";
import Home from "../page_objects/usersPage"
const home = new Home


And('acesso com usuario {string} e {string}', (User,Password) =>{
    home.typeUser(User, Password);
})
When('seleciono para entrar', () =>{
    home.submitAzure();
})
Then('verifico que o sistema entrou e exibe uma mensagem {string}', (checkpoint) =>{
    home.dashboardScreen(checkpoint);
})
Then('o usuario não entra e aparece uma mensagem {string}', (checkpoint) =>{
    home.screenError(checkpoint);
})