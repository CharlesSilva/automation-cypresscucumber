Feature: Validação de usuários do sistema azureWebSite

    Como usuario, desejo acessar o site itera-qa com o usuario cadastrado
    para que eu possa ter a rastreabilidade e o controle de cadastro de clientes

    Background: Acess Web Site
        Given acesso o site itera-qa
        And seleciono para acessar via login

    Scenario Outline: teste para validar login de usuario "<Perfil>"
        And acesso com usuario "<User>" e "<Password>"
        When seleciono para entrar
        Then verifico que o sistema entrou e exibe uma mensagem "<checkpoint>"

        Examples:
            | Perfil | User         | Password | checkpoint           |
            | Valido | CharlesSilva | 12345    | Welcome CharlesSilva |

    Scenario Outline: Validar as informações "<Perfil>"
        And acesso com usuario "<User>" e "<Password>"
        When seleciono para entrar
        Then o usuario não entra e aparece uma mensagem "<checkpoint>"

        Examples:
            | Perfil                               | User          | Password | checkpoint                 |
            | Usuario invalido                     | CharlessSilva | 12345    | Wrong username or password |
            | Senha invalida                       | CharlesSilva  | 123456   | Wrong username or password |
            | Usuario e senha invalidas            | CharlessS     | 123456   | Wrong username or password |
            | Usuario vazio e senha valida         |               | 12345    | Wrong username or password |
            | Usuario valido e senha vazia         | CharlesSilva  |          | Wrong username or password |
            | Usuario vazio e senha vazia          |               |          | Wrong username or password |
            | Usuario e senha com caractere        | @$%%#         | @#$*!!   | Wrong username or password |
            | Usuario com caractere e senha valida | @$%%#         | 12345    | Wrong username or password |
            | Usuario valido e senha com caractere | CharlesSilva  | @#$*!!   | Wrong username or password |
