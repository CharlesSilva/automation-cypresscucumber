Feature: Validação de cadastro de cliente

     Como usuario, desejo acessar o site itera-qa com o usuario cadastrado
     para que eu possa ter a rastreabilidade e o controle de cadastro de clientes
     
     Background: Acess Web Site
        Given acesso o site itera-qa
        And seleciono para acessar via login

        Scenario Outline: Validar cadastro realizado em Dashboard
        And acesso com usuario "<User>" e "<Password>" entrando na home Dashboard
        And em Dashboard faço um cadastro de cliente
        When faço a pesquisa do cliente cadastrado
        Then verifico que exibe o cliente na tabela de Detalhes de cliente

        Examples:
            | User          | Password |
            | CharlesSilva  | 12345    |




