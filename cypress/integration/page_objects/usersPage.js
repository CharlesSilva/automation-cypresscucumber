import Base from "./basePage"

const validationHome = '.jumbotron h1'
const btnLog = 'a[href="/Login"]'
const valueUser = 'input[id="Username"]'
const valuePassword = 'input[id="Password"]'
const btnSubmit = '[name="login"]'
const validationDash = '[class="container body-content"] h3'
const btnCreate = '[class="btn btn-primary"]'
const valueName = '[id="Name"]'
const valueCompany = '[id="Company"]'
const valueAddress = '[id="Address"]'
const valueCity = '[id="City"]'
const valuePhone = '[id="Phone"]'
const valueEmail = '[id="Email"]'
const btnFinalist = '[class="btn btn-primary"]'
const valueCustumer = '[id="searching"]'
const btnSearch = 'form [value="Search"]'
const validationSearch = 'body tr:nth-child(2)>td:nth-child(1)'
const btnDelete = '[class="btn btn-outline-danger"]'
const btnDelete2 = '[value="Delete"]'
const msgErrorUser = '[class="alert-danger"]'

export default class Home {
    
    accessWeb(){
       cy.visit('/home')
       Base.verifyElementExist(validationHome)
    }
    loginScreen(){
       Base.clickElement(btnLog)
       cy.visit('/Login')
    }
    typeUser(User, Password){
       Base.typeValue(valueUser, User)
       Base.typeValue(valuePassword, Password)
    }
    submit(){
        Base.clickElement(btnSubmit)
        Base.verifyElementExist(validationDash)
        Base.clickElement(btnCreate)
    }
    createNew(){
        Base.typeValue(valueName, 'Jhon Silva')
        Base.typeValue(valueCompany, 'AnalistQA')
        Base.typeValue(valueAddress, 'Av.Brasil')
        Base.typeValue(valueCity, 'Manaus')
        Base.typeValue(valuePhone, '92 99999999')
        Base.typeValue(valueEmail, 'cl@gmail.com')
        Base.clickElement(btnFinalist)
    }
    searchCustomer(){
        Base.typeValue(valueCustumer, 'Jhon Silva')
        Base.clickElement(btnSearch)
    }
    searchValidation(){
        Base.verifyElementExist(validationSearch)
        Base.clickElement(btnDelete)
        Base.clickElement(btnDelete2)
    }
    submitAzure(){
        Base.clickElement(btnSubmit)
    }
    dashboardScreen(){
        Base.verifyElementExist(validationDash)
    }
    screenError(){
        Base.verifyElementExist(msgErrorUser)
    }
    emptyField(){
        Base.verifyElementExist(msgErrorUser)
    }

}